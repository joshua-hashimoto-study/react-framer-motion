import React from "react";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";

const containerVariants = {
    init: {
        opacity: 0,
        x: "100vw",
    },
    animate: {
        opacity: 1,
        x: 0,
        transition: {
            type: "spring",
            delay: 0.5,
        },
    },
    exit: {
        x: "-100vw",
        transition: {
            ease: "easeInOut",
        },
    },
};

const nextVariants = {
    init: {
        x: "-100vw",
    },
    animate: {
        x: 0,
        transition: {
            type: "spring",
            stiffness: 120,
        },
    },
};

const buttonVariants = {
    // animate: {
    //     x: [0, -20, 20, -20, 20, 0],
    //     transition: {
    //         delay: 2,
    //     },
    // },
    hover: {
        scale: 1.1,
        textShadow: "0px 0px 8px rgb(255, 255, 255)",
        boxShadow: "0px 0px 8px rgb(255, 255, 255)",
        transition: {
            // yoyo: 10, // keyframeの数
            yoyo: Infinity,
            duration: 0.3,
        },
    },
};

// variantsのプロパティ名が親子共に同じなら、親のアトリビュートを指定するだけで良い。子は親で設定されているアトリビュートと同じ名前のvariantを探す。
// プロパティ名が違う場合は別途指定する必要がある

const Base = ({ addBase, pizza }) => {
    const bases = ["Classic", "Thin & Crispy", "Thick Crust"];

    return (
        <motion.div
            className="base container"
            variants={containerVariants}
            initial="init"
            animate="animate"
            exit="exit"
        >
            <h3>Step 1: Choose Your Base</h3>
            <ul>
                {bases.map((base) => {
                    let spanClass = pizza.base === base ? "active" : "";
                    return (
                        <motion.li
                            key={base}
                            onClick={() => addBase(base)}
                            whileHover={{
                                scale: 1.3,
                                color: "#f8e112",
                                originX: 0,
                            }}
                            transition={{
                                type: "spring",
                                stiffness: 300,
                            }}
                        >
                            <span className={spanClass}>{base}</span>
                        </motion.li>
                    );
                })}
            </ul>

            {pizza.base && (
                <motion.div
                    className="next"
                    variants={nextVariants}
                    // initial="init"
                    // animate="animate"
                >
                    <Link to="/toppings">
                        <motion.button
                            variants={buttonVariants}
                            whileHover="hover"
                        >
                            Next
                        </motion.button>
                    </Link>
                </motion.div>
            )}
        </motion.div>
    );
};

export default Base;
