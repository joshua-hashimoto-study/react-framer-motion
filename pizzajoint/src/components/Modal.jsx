import React from "react";
import { Link } from "react-router-dom";
import { motion, AnimatePresence } from "framer-motion";

const backdropVariants = {
    init: {
        opacity: 0,
    },
    animate: {
        opacity: 1,
    },
};

const modalVariants = {
    init: {
        y: "-100vh",
        opacity: 0,
    },
    animate: {
        opacity: 1,
        y: "200px",
        transition: {
            delay: 0.5,
        },
    },
};

const Modal = ({ showModal }) => {
    return (
        <AnimatePresence exitBeforeEnter>
            {showModal && (
                <motion.div
                    className="backdrop"
                    variants={backdropVariants}
                    initial="init"
                    animate="animate"
                    exit="init"
                >
                    <motion.div className="modal" variants={modalVariants}>
                        <p>Want to make another pizza?</p>
                        <Link to="/">
                            <button>Start Again</button>
                        </Link>
                    </motion.div>
                </motion.div>
            )}
        </AnimatePresence>
    );
};

export default Modal;
